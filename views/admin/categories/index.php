<section class="title">
	<h4><?php echo lang('calendar:item_list'); ?></h4>
</section>

<section class="item">
	<div class="content">
	<?php echo form_open('admin/calendar/categories/delete');?>
	
	<?php if (!empty($categories)): ?>
	
		<table>
			<thead>
				<tr>
					<th><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
					<th><?php echo lang('calendar:name'); ?></th>
					<th><?php echo lang('calendar:description'); ?></th>
					<th></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="5">
						<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
					</td>
				</tr>
			</tfoot>
			<tbody>
				<?php foreach( $categories as $categorie ): ?>
				<tr>
					<td><?php echo form_checkbox('action_to[]', $categorie->id); ?></td>
					<td><?php echo $categorie->name; ?></td>
					<td><?php echo $categorie->description; ?></td>
					<td class="actions">
						<?php echo	anchor('admin/calendar/categories/edit/'.$categorie->id, lang('calendar:edit'), 'class="btn orange"'); ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		
		<div class="table_action_buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))); ?>
		</div>
		
	<?php else: ?>
		<div class="no_data"><?php echo lang('sample:no_items'); ?></div>
	<?php endif;?>
	
	<?php echo form_close(); ?>
	</div>
</section>
