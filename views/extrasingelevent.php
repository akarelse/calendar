{{ if exists item }}
<table class="contentpaneopen" border="0">
    <tbody>
        <tr class="headingrow">
            <td width="100%" class="contentheading">
                <h2 class="contentheading">
                    {{ item.name }}
                </h2>
            </td>
        </tr>
        <tr align="left" valign="top">
            <td colspan="4">
                <p>
                   {{ item.description }}
                </p>
            </td>
        </tr>
        {{ if item.location != "" }}
        <tr>
            <td class="ev_detail" align="left" valign="top" colspan="4">
                <b>Locatie :</b> {{ item.location }}<br>
            </td>
        </tr>
        {{ endif }}
        <tr>
            <td class="ev_detail" align="left" valign="top" colspan="4">
                <b>Tijd: </b> {{ item.startday }} {{ item.starthourmin }} uur tot {{ if item.diffdays > 0 }} {{ item.stopday }} {{ endif }} {{ item.stophourmin }} uur<br>
               {{ if item.price > 0 }} <b>Prijs: </b> €{{ item.price }}<br>{{ endif }}
            </td>
        </tr>
    </tbody>
</table>
{{ else }}
<p>
    This is not an event.
</p>
{{ endif }}