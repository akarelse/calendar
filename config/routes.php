<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['(calendar)/(:num)/(:num)/(:any)'] = 'calendar/view/$4';
$route['(calendar)/day/(:num)/(:num)/(:num)'] = 'calendar/day/$2/$3/$4';
$route['(calendar)/week/'] = 'calendar/week';
$route['(calendar)/(:num)/(:num)'] = 'calendar/index';
$route['(calendar)/(:num)'] = 'calendar/event/$2';
$route['calendar/admin/categories(:any)?'] = 'admin_categories$1';



